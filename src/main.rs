use ansi_term::Colour::*;
use dirs;
use libc;
use mio::{unix::EventedFd, Events, Poll, PollOpt, Ready, Token};
use serde::{Deserialize, Serialize};
use std::env;
use std::error::Error;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, Read, Write};
use std::os::unix::io::AsRawFd;
use std::path::PathBuf;
use std::process::Command;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;
use toml;
#[derive(Deserialize, Serialize, Debug)]
struct FakeShellConfig {
    allowed: Vec<String>,
    welcome: String,
    last_command_result: i32,
}

impl Default for FakeShellConfig {
    fn default() -> Self {
        let mut fake = Self { allowed: vec!(),
                last_command_result: 0,
                welcome: String::from("Welcome to fakeshell! You are not allowed to bash the system up because this is fakeshell!") };
        fake.allowed.push("exit".into());
        fake.allowed.push("echo".into());
        fake.allowed.push("cd".into());
        fake.allowed.push("env".into());
        fake.allowed.push("df".into());
        fake.allowed.push("pwd".into());
        fake.allowed.push("ls".into());
        fake.allowed.push("help".into());
        fake.allowed.push("$?".into());
        fake.allowed.push("cat".into());
        fake.allowed.push("export".into());
        fake
    }
}

#[derive(PartialEq)]
enum FakeShellStatus {
    Continue,
    Exit,
}

struct Cd {
    _allowed: Vec<String>,
}

struct Exec;

impl Exec {
    fn new() -> Self {
        Self {}
    }

    fn exec(&self, cmd: &str, args: Vec<String>) -> i32 {
        match Command::new(cmd).args(args).output() {
            Ok(output) => {
                let out = String::from_utf8_lossy(&output.stdout);
                print!("{}", out);
                0
            }
            Err(e) => {
                eprintln!("{}", Red.paint(e.description()));
                255
            }
        }
    }
}

impl Cd {
    fn new() -> Self {
        Self { _allowed: Vec::new() }
    }

    fn cd(&self, path: PathBuf) -> Result<i32, i32> {
        env::set_current_dir(&path)
            .map_err(|e| {
                eprintln!("{}", Red.paint(e.description()));
                1
            })
            .map(|_| 0 as i32)
    }
}

struct Pwd;
impl Pwd {
    fn pwd() -> i32 {
        let mut res = 0;
        println!(
            "{}",
            env::current_dir()
                .unwrap_or_else(|e| {
                    eprintln!("{}", e);
                    res = 1;
                    PathBuf::new()
                })
                .to_string_lossy()
        );
        res
    }
}

impl FakeShellConfig {
    fn new(config: Option<PathBuf>) -> Self {
        let mut fake = FakeShellConfig::default();
        //let mut fname: PathBuf;
        let fname = if let Some(fname) = config {
            fname
        } else {
            let mut fname = dirs::config_dir().unwrap();
            fname.push("fakeshell/config.toml");
            fname
        };

        File::open(&fname)
            .and_then(|mut file| {
                let mut data = Vec::new();
                file.read_exact(&mut data).unwrap();
                match toml::from_slice(&data[0..]) {
                    Ok(res) => fake = res,
                    Err(e) => eprintln!("{:?}", e),
                };

                Ok(())
            })
            .unwrap_or_else(|e|{
                eprintln!("Read config at '{:?}' failed: {}", fname, e);
            });
        let mut path = dirs::config_dir().unwrap();
        path.push("fakeshell");
        std::fs::create_dir_all(&path).unwrap();
        File::create(&fname)
            .and_then(|mut file| {
                file.write_all(toml::to_string_pretty(&fake).unwrap().as_bytes())?;
                Ok(file)
            })
            .expect("file failed");

        println!("{}", fake.welcome);
        println!("Commands available:\n{:?}", fake.allowed);
        fake
    }

    fn is_allowed(&self, cmd: &str) -> bool {
        for allow in self.allowed.iter() {
            if allow == cmd {
                return true;
            }
        }
        false
    }

    fn process(&mut self, args: String) -> FakeShellStatus {
        // Get rid of whitespaces at start and end then split arguments.
        let mut args = args.trim().split_whitespace();
        // First is always cmd
        let cmd = args.next().unwrap_or("");
        if cmd.is_empty() {
            println!();
            self.last_command_result = 0;
            return FakeShellStatus::Continue;
        }
        // Is the command allowed?
        if !self.is_allowed(cmd) {
            // For security reasons we just print command not found
            // and does not differentiate if from the "real"
            // command not found error message.
            self.last_command_result = 127;
            eprintln!("{}: Command not found", cmd);
            return FakeShellStatus::Continue;
        }

        self.last_command_result = match cmd {
            "$?" => {
                println!("{}", self.last_command_result);
                0
            }
            "pwd" => Pwd::pwd(),
            "cat" => {
                let fname: PathBuf = args
                    .map(|arg| {
                        let res: String = arg
                            .find('$')
                            .map(|offset| {
                                env::var(&arg[offset + 1..])
                                    .map(|value| format!("{}{}", &arg[0..offset], value))
                                    .unwrap_or_else(|_| arg.into())
                            })
                            .unwrap_or_else(|| arg.into());
                        PathBuf::from(&res)
                    })
                    .collect();
                File::open(fname)
                    .map(|file| {
                        let buf = BufReader::new(file);
                        for line in buf.lines() {
                            line.map(|line| {
                                println!("{}", line);
                            })
                            .unwrap_or(());
                        }
                        0 as i32
                    })
                    .unwrap_or_else(|e| {
                        eprintln!("{}", e);
                        1 as i32
                    })
            }
            "env" => {
                for (key, value) in env::vars_os() {
                    println!(
                        "{}={}",
                        key.into_string().unwrap(),
                        value.into_string().unwrap()
                    );
                }
                0
            }
            "echo" => {
                let all: String = args
                    .map(|arg| {
                        let res: String = arg
                            .find('$')
                            .map(|offset| {
                                env::var(&arg[offset + 1..])
                                    .map(|value| format!("{}{}", &arg[0..offset], value))
                                    .unwrap_or_else(|_| arg.into())
                            })
                            .unwrap_or_else(|| arg.into());
                        format!("{} ", &res)
                    })
                    .collect();
                println!("{}", all);
                0
            }
            "exit" => {
                println!("Bye");
                return FakeShellStatus::Exit;
            }
            "help" => {
                for allow in &self.allowed {
                    println!("{}", allow);
                }
                0
            }
            "cd" => {
                let dir = args.next()
                    .map(PathBuf::from)
                    .unwrap_or_else(||
                                 dirs::home_dir().expect("Your are running fakeshell on an unsupported platform where environment variable HOME is None?") );

                Cd::new()
                    .cd(PathBuf::from(&dir))
                    .unwrap_or_else(|e| e as i32)
            }
            "export" => {
                let all: String = args
                    .map(|arg| {
                        let res: String = arg
                            .find('$')
                            .map(|offset| {
                                env::var(&arg[offset + 1..])
                                    .map(|value| format!("{}{}", &arg[0..offset], value))
                                    .unwrap_or_else(|_| arg.into())
                            })
                            .unwrap_or_else(String::new);
                        format!("{} ", &res)
                    })
                    .collect();
                let mut keyval = all.split('=');
                if let Some(key) = keyval.next() {
                    let value = keyval
                        .next()
                        .map(|val| val.to_string())
                        .unwrap_or_else(String::new);

                    env::set_var(key, value);
                }
                0 as i32
            }
            cmd => {
                let args: Vec<String> = args.map(String::from).collect();
                Exec::new().exec(cmd, args)
            }
        };

        FakeShellStatus::Continue
    }

    fn read_line(&mut self) -> FakeShellStatus {
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(len) if len > 0 => self.process(input),
            Ok(_) => FakeShellStatus::Exit,
            Err(_) => FakeShellStatus::Continue,
        }
    }
}

fn main() {
    let sigterm = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(libc::SIGQUIT, sigterm.clone()).unwrap();
    //signal_hook::flag::register(libc::SIGINT, sigterm.clone()).unwrap();
    let mut fakeshell = FakeShellConfig::new(None);
    let mut terminate = false;
    let tok = Token(0);
    let poll = Poll::new().unwrap();
    poll.register(
        &EventedFd(&io::stdin().as_raw_fd()),
        tok,
        Ready::readable(),
        PollOpt::edge(),
    )
    .unwrap();
    let mut events = Events::with_capacity(5);
    io::stdout()
        .write_all(b"Fakeshell # ")
        .unwrap_or_else(|e|{
            eprintln!("Write to stdout failed? {}", e);
        });
    io::stdout()
        .flush()
        .unwrap_or_else(|e| eprintln!("Flush failed {}", e));

    while !terminate && !sigterm.load(Ordering::Relaxed) {
        poll.poll(&mut events, Some(Duration::from_millis(100)))
            .unwrap();
        for _ in &events {
            if fakeshell.read_line() == FakeShellStatus::Exit {
                terminate = true;
                break;
            }
            io::stdout()
                .write_all(b"Fakeshell # ")
                .unwrap_or_else(|e| eprintln!("Write to stdout failed? {}", e));
            io::stdout()
                .flush()
                .unwrap_or_else(|e| eprintln!("Flush failed {}", e));
        }
    }
    println!("{}", sigterm.load(Ordering::Relaxed));
}
