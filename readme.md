# Fakeshell

Just a fake shell

Basic shell commands supported:

 - cat (very basic)
 - echo
 - set/get environment similar to bash.
 - pwd
 - cd
 - help

Non builtin commands (using installed ls/df):

 - ls
 - df


## Dependies

* StructOpt
* serde
* toml

